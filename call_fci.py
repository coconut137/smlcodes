from pyscf import fci as pyfci
import libdmet.utils.logger as log
import numpy as np


class fci_solver(object):
    def __init__(self, norb=None, nelec=None, civector=None, restricted=False,\
            bogoliubov=False):
        log.info("Using pyscf fci as the impurity solver")
        log.eassert(bogoliubov==False, "Particle symmetry breaking version is not implemented!\n")
        self.norb = norb
        self.nelec = nelec
        self.civector = civector
        self.restricted = restricted

    def kernel(self, Ham):
        norb = Ham.norb
        self.norb = norb
        nelec = self.nelec
        if nelec == None:
            nelec = norb
            self.nelec = nelec
        h1e = Ham.H1['cd']
        h2e = Ham.H2['ccdd']
        if self.restricted:
            log.warning("FCI for restricted spin is not implemented yet\n!")
            log.warning("setting restricted to False!")
            self.restricted = False
        else:
            h2e[[1,2],:] = h2e[[2,1],:] #the order pyscf stored h2e is different
            Eemb, civec = pyfci.direct_uhf.kernel(h1e, h2e, norb, nelec)
        self.civector = civec
        return Eemb, civec

    def fci_rdm1s(self):
        log.eassert(self.civector is not None, "The fci_solver needs to be called first to initialize the solver!\n")
        norb = self.norb
        nelec = self.nelec
        civec = self.civector
        rdm1a, rdm1b = pyfci.direct_spin1.make_rdm1s(civec, norb, nelec) #tuple : alpha, beta
        log.section("Embbeding 1-particle density matrices (alpha, beta):\n")
        log.section("%s\n%s\n"%(rdm1a, rdm1b))
        return rdm1a, rdm1b
        
    def fci_rdm12s(self):
        log.eassert(self.civector is not None, "The fci_solver needs to be called first to initialize the solver!\n")
        norb = self.norb
        nelec = self.nelec
        civec = self.civector
        (dm1a, dm1b), (dm2aa, dm2ab, dm2bb) = pyfci.direct_spin1.make_rdm12s(civec, norb, nelec)
        return np.asarray([dm1a, dm1b]), np.asarray([dm2aa, dm2bb, dm2ab]) # the order changed
    
    def fci_rdm2s(self):
        return self.rdm12s()[1]

    def run(self, Ham, nelec=None, norb=None, restricted=False):
        Eimp, _ = self.kernel(Ham)
        rdm1a, rdm1b = self.fci_rdm1s()
        return Eimp, np.asarray([rdm1a, rdm1b])

    def cleanup(self):
        pass


if __name__ == "__main__":
    fci=fci_solver()
