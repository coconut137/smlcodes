SmlCodes
========

<p>2016-09-16 

California Institute of Technology

Author: Chong Sun (sunchong137@gmail.com)

A collection of small scripts implementing numerical methods used in my project.

Including:

* Lanczos Algorithm

* DIIS

* Block average code by Ushnish Ray
