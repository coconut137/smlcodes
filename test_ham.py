import libdmet.system.hamiltonian
import libdmet.dmet.Hubbard as dmet
import libdmet.routine.bcs_helper as bcs_helper
from libdmet.routine.bcs import transformResults
import numpy as np
import call_fci

def test():
    LatSize = [4, 4]
    ImpSize = [2, 1]
    Lat = dmet.SquareLattice(*(LatSize + ImpSize))
    Filling = 1.0/2 # for symmetric hartree-fock
    U = 4.
    Mu = U * Filling
    M = 600
    vcor = dmet.InitGuess(ImpSize, U)
    print "vcor:\n", vcor.get()

    Ham = dmet.Ham(Lat, U) 
    Lat.setHam(Ham)
    rho, Mu = dmet.HartreeFock(Lat, vcor, Filling, Mu)
#    rho, Mu = dmet.HartreeFock(Lat, vcor, Mu)

    ImpHam, H_energy, basis = dmet.ConstructImpHam(Lat, rho, vcor)  
    print ImpHam.norb
    # H1e here is Int1e energy
    h1e = ImpHam.H1['cd']
    h2e = ImpHam.H2['ccdd']
    h2e[[1,2],:]=h2e[[2,1],:]
    fci=call_fci.fci_solver()
    Eemb, rhoEmb = fci.run(ImpHam)
    rhoImp, Eimp, nelec_imp = dmet.transformResults(rhoEmb, Eemb, basis, ImpHam, H_energy)

    print rhoImp
    exit()

    rdm1 = list(rdm1)
    rdm1 = np.asarray(rdm1)
    grhoA, grhoB, kappaAB = bcs_helper.extractRdm(rdm1)
    
    solver = dmet.impurity_solver.StackBlock(nproc = 1, nthread = 1, \
        nnode = 1, bcs = False, reorder = False, tol = 1e-7, maxM = M,
        TmpDir = "/home/sunchong/scratch/dmet", SharedDir = "/home/sunchong/scratch/gpfs/dmet")

    onepdm, Edmrg = solver.run(ImpHam, nelec=4) 
    print onepdm.shape
    grhoA2, grhoB2, _ = bcs_helper.extractRdm(onepdm)
    print grhoA-grhoA2
    print grhoB-grhoB2
    print rhoA
#    print E-Edmrg
#    print rdm1-onepdm
if __name__ == "__main__":
    test()
