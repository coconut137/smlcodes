#!/usr/local/bin/python

import numpy as np
import matplotlib.pyplot as plt
from glob import glob
from matplotlib.pyplot import cm
from matplotlib import rcParams
import sys
rcParams.update({'figure.autolayout': True})

def plt_block(solver):
    Eprof=np.load("./data/Eprofile_%s.npy"%solver)
    npt = len(Eprof)
    x = np.asarray(range(len(Eprof.T)))-len(Eprof.T)/2.
    color = cm.rainbow(np.linspace(0.1, 0.9, npt))
    for i, clr in zip(range(npt), color):
        plt.plot(x, Eprof[i, :], c=clr, lw=3, label="%d"%i)
        plt.scatter(x, Eprof[i, :], c=clr, s = 15)
#       if i == n:
#           plt.plot(x, Eprof[i, :], c=clr, lw=3, label="%d"%i)
#           plt.scatter(x, Eprof[i, :], c=clr, s = 15)
#           
    plt.ylabel(r"$E_{imp}$", fontsize=20)
    plt.xlabel(r"$N_{steps}$", fontsize=20)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.xlim(-101,103)
    plt.legend(loc="upper right")
    plt.savefig("./data/Eprof_%s.png"%solver, dpi=300)
#   plt.savefig("./data/dmrg/noise_off_3_%d.png"%n, dpi=300)
    plt.show()
    
if __name__ == "__main__":
    plt_block(sys.argv[1])
