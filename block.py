# block average code
# Chong Sun
import numpy as np

def block_average(fname, lblk_m = None, lblk_M = None):
    '''
        fname     --  an .npy file, saving the data to be block averaged
        lblk_m    --  smallest length of per block
        lblk_M    --  largest length of per block
    '''
    try:
        data = np.load(fname) #1D array
    except:
        data = fname
    l_data = len(data)
    if lblk_m==None:
        lblk_m = 2
    if lblk_M==None:
        lblk_M = l_data//32 #we need at least 64 blocks
    if lblk_m >= lblk_M:
        raise("The number of data is insufficient!")
        
    nblk = l_data//lblk_m
    dev = []
    while nblk >= (l_data//lblk_M):
        lblk = l_data//nblk
        res = []
        for i in range(nblk):
            res.append(np.mean(data[i*lblk:(i+1)*lblk]))
        res = np.asarray(res)
        err = np.std(res)/(nblk-1)**0.5
        unc = err/(2*(nblk-1))**0.5
        dev.append([lblk, err, unc])
#        print lblk, unc
        nblk /= 2
        
    dev = np.asarray(dev)
    return dev


if __name__ == "__main__":
    data = np.random.randn(2000)
    dev = block_average(data)
    print dev

