'''diis algorithm
    Chong Sun 09-06-2016
    The residual should be provided from outside.
'''
from scipy.optimize import minimize as sp_minimize
import numpy as np
from numpy import linalg as nl
from scipy.linalg import lu


def diis_main(startdim, Mdim, p0, grad, tol=1e-7, MaxIter=30):
    '''
        startdim : the minimum dimension to start diis procedure, usually small
        Mdim : the dimension of the diis subspace
        p0 : initial point (vector)
        grad : function that returns the gradient at a given point p
        gtol : the tolerance that if grad(p) < gtol, the procedure is converged
        MaxIter : the minimum
    '''
    N = len(p0)
    if Mdim >= len(p0):
        raise Exception('max dimension of the diis subspace is larger than the dimension of the space')
    mindim = startdim
    maxdim = Mdim
    pk = np.asarray(p0).copy()
    jac = grad # we copy the arguments to be safe
    rspace = [] # storing the residuals
    pspace = [] # storing the vectors
    #initialize the pspace and rspace
    cnt1 = 0
    for iter in range(mindim):
        rkp1 = -grad(pk)
        pkp1 = pk - rkp1
        lid = linedep(rkp1, rspace)
        if lid:
            rspace.append(rkp1.copy())
            pspace.append(pkp1.copy())
        else:
            iter -= 1
        cnt1 += 1
        if cnt1 == 2*N:
            raise Exception('The initialization cannot be done!')
    
    cnt2 = 0
    while(cnt2 < MaxIter):       
        rkp1 = -grad(pkp1)    
        if nl.norm(rkp1) <= tol:
            print("Diis converged after %d iterations."%cnt2)
            break
        lid = linedep(rkp1, rspace) # linear independent
        if lid:
            if len(rspace) == maxdim:
                rspace.pop(0)
                pspace.pop(0)
            rspace.append(rkp1.copy())
            pkp1 -= rkp1
            pspace.append(pkp1.copy())
            cvec = minimize_coef(rspace)
            pkp1 = np.asarray(pspace).T.dot(cvec) # new pkp1
            pspace[-1]=pkp1.copy()
           
        cnt2 += 1
    return pkp1       


def linedep(x, xspace):
    Xspace = np.asarray(xspace)
    if len(Xspace.shape) == 1:
        return True
    Xspace = np.vstack([Xspace, x])
    pl, u = lu(Xspace, permute_l=True)
    if nl.norm(u[-1]) == 0:
        return False
    else:
        return True
# suppose that the gradient evaluation function is grad()


def minimize_coef(rspace, tol=1e-8):
    def costfunc(cvec, _alpha=1.):
        Rspace = np.asarray(rspace)
        res_norm = nl.norm((Rspace.T).dot(cvec))
        return res_norm + _alpha * (np.sum(cvec)-1)**2.
    
    def gradient(cvec, _alpha):
        Rspace = np.asarray(rspace)
        deno = costfunc(cvec)
        nume = Rspace.dot(Rspace.T.dot(cvec))
        return (1.*nume)/deno + 2* _alpha *(np.sum(cvec)-1)
    
    notconv = True
    alpha = 1.
    beta = 3.
    cnt = 0
    while notconv: # using penalty method
        print "penalty parameter: ", alpha
        c0 = np.random.rand(len(rspace))
#        c0[-1] = 1.
        def cf(cvec):
            return costfunc(cvec, alpha)
        def gd(cvec):
            return gradient(cvec, alpha)
        cvec = sp_minimize(cf, c0, jac=gd).x
        if abs(np.sum(cvec)-1.) <= tol:
            notconv = False
            break
        else:
            alpha *= beta
        cnt += 1
        if cnt >= 10:
            print("The constraint is not zero: %f"%abs(np.sum(cvec)-1.))
            break
    print "the coefficient is \n", cvec
    return cvec



if __name__ == "__main__":
    x0 = [-1.3, 0.7, 0.8, 1.9, 1.2]
    #from scipy.optimize import rosen_der
    def gradient(x):
        gx = 2 * x
        gx[1] = (x[1]-1.) * 2
        return gx
    print diis_main(1,3,x0,gradient)

