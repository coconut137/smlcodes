import libdmet.system.hamiltonian
import libdmet.dmet.Hubbard as dmet
import numpy as np
from copy import deepcopy
import libdmet.utils.logger as log
import call_fci

LatSize = [4, 4]
ImpSize = [2, 1]
Lat = dmet.SquareLattice(*(LatSize + ImpSize))
Filling = 1.0/2 # for symmetric hartree-fock
U = 4.
Mu = U * Filling
M = 600
vcor = dmet.InitGuess(ImpSize, U)
Ham = dmet.Ham(Lat, U) 
Lat.setHam(Ham)
solver = call_fci.fci_solver()

def FitVcor(vcor, Mu, solver, lattice, filling):
    nscsites=lattice.supercell.nsites
    def costfunc(param, updt=True, v=False):
        if updt:
            vcor.update(param)

        vcor_tmp = deepcopy(vcor)
        vcor_tmp.update(param)
        rhoHF, mu = dmet.HartreeFock(lattice, vcor_tmp, filling, Mu)
        ImpHam, H_energy, basis = dmet.ConstructImpHam(lattice, rhoHF, vcor_tmp)
        Eemb, rhoEmb = solver.run(ImpHam)
        rhoImp, Eimp, nelec_imp = dmet.transformResults(rhoEmb, Eemb, basis, ImpHam, H_energy)
        rhoAT, rhoBT = rhoHF[0], rhoHF[1]
        rhoAImp, rhoBImp = rhoImp[0], rhoImp[1]
        av_rhoAT, av_rhoBT = np.mean(rhoAT, axis=0), np.mean(rhoBT, axis=0)

#       constraint = np.sum((vcor_tmp.get()[0]-mu*np.eye(nscsites)) * (rhoAT[0] - rhoAImp)) + \
#       np.sum((vcor_tmp.get()[1]-mu*np.eye(nscsites)) * (rhoBT[0] - rhoBImp)) 

        constraint = np.sum((vcor_tmp.get()[0]-mu*np.eye(nscsites)) * (av_rhoAT - rhoAImp)) + \
        np.sum((vcor_tmp.get()[1]-mu*np.eye(nscsites)) * (av_rhoBT - rhoBImp)) 
        if v:
            return Eimp, constraint
        else:
            return Eimp+constraint**2 
        
    def grad(param, dx=1e-4):
        res = np.empty_like(param)
        for iter in range(len(param)):
            lparam, rparam = param.copy(), param.copy()
            lparam[iter] -= dx
            rparam[iter] += dx
            res[iter] = (costfunc(rparam, updt = False) - costfunc(lparam, updt = False))/(2.*dx)
        log.section("\n Gradient:\n%s"%res)
        return res
 
    from scipy.optimize import minimize
    e_begin, c_begin = costfunc(vcor.param, updt=False, v=True) 
    log.info("begin: \nimpurity energy = %20.12f    constraint = %20.12f", e_begin, c_begin)
    param = minimize(costfunc, vcor.param, jac=grad, options={"disp": True, "maxiter": 10}).x
    e_end, c_end = costfunc(param, updt=False, v=True)
    log.info("end: \n impurity energy = %20.12f    constraint = %20.12f", e_end, c_end)
    vcor.update(param)
    return vcor, c_begin, c_end
    
        
if __name__ == "__main__":
    LatSize = [4, 4]
    ImpSize = [2, 1]
    Lat = dmet.SquareLattice(*(LatSize + ImpSize))
    Filling = 1.0/2 # for symmetric hartree-fock
    U = 4.
    Mu = U * Filling
    M = 600
    vcor = dmet.InitGuess(ImpSize, U)
    Ham = dmet.Ham(Lat, U) 
    Lat.setHam(Ham)
    solver = call_fci.fci_solver()

    FitVcor(vcor, Mu, solver, Lat, Filling)
